output "vpc-1-id" {
  description = "vpc1 id"
  value       = "${aws_vpc.vpc-1.id}"
}

output "vpc-10-id" {
  description = "vpc10 id"
  value       = "${aws_vpc.vpc-10.id}"
}

output "vpc-20-id" {
  description = "vpc20 id"
  value       = "${aws_vpc.vpc-20.id}"
}

output "vpc-30-id" {
  description = "vpc30 id"
  value       = "${aws_vpc.vpc-30.id}"
}

output "subnet_ids" {
  description = "subnet_ids"
  value       = [
      "${aws_subnet.vpc-1-sub-pub-a.id}",
      "${aws_subnet.vpc-1-sub-pub-b.id}",
      "${aws_subnet.vpc-1-sub-priv-a.id}",
      "${aws_subnet.vpc-1-sub-priv-b.id}",
      "${aws_subnet.vpc-1-sub-mgmt-a.id}",
      "${aws_subnet.vpc-1-sub-mgmt-b.id}",
      "${aws_subnet.vpc-10-sub-a.id}",
      "${aws_subnet.vpc-10-sub-b.id}",
      "${aws_subnet.vpc-20-sub-a.id}",
      "${aws_subnet.vpc-20-sub-b.id}",
      "${aws_subnet.vpc-30-sub-a.id}",
      "${aws_subnet.vpc-30-sub-b.id}"
      ]
}

output "vpc-10-subnets-ids" {
  description = "vpc-10-subnets-ids"
  value       = ["${aws_subnet.vpc-10-sub-a.id}", "${aws_subnet.vpc-10-sub-b.id}"]
}
