###############
# VPC Section #
###############

# VPCs

resource "aws_vpc" "vpc-1" {
  cidr_block = "10.1.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc1-shared"
    Owner = "${var.USERNAME}"
    scenario = "${var.scenario}"
    env = "shared"
  }
}

resource "aws_vpc" "vpc-10" {
  cidr_block = "10.10.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc1-dev"
    Owner = "${var.USERNAME}"
    scenario = "${var.scenario}"
    env = "dev"
  }
}

resource "aws_vpc" "vpc-20" {
  cidr_block = "10.20.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc2-preprod"
    Owner = "${var.USERNAME}"
    scenario = "${var.scenario}"
    env = "dev"
  }
}

resource "aws_vpc" "vpc-30" {
  cidr_block = "10.30.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc3-prod"
    Owner = "${var.USERNAME}"
    scenario = "${var.scenario}"
    env = "shared"
  }
}

# Subnets

resource "aws_subnet" "vpc-1-sub-pub-a" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-pub-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-1-sub-pub-b" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-pub-b"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-1-sub-mgmt-a" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.101.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-mgmt-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-1-sub-mgmt-b" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.102.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-mgmt-b"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-1-sub-priv-a" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.201.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-priv-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-1-sub-priv-b" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.1.202.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-priv-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-10-sub-a" {
  vpc_id     = "${aws_vpc.vpc-10.id}"
  cidr_block = "10.10.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-10.tags.Name}-sub-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-10-sub-b" {
  vpc_id     = "${aws_vpc.vpc-10.id}"
  cidr_block = "10.10.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-10.tags.Name}-sub-b"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-20-sub-a" {
  vpc_id     = "${aws_vpc.vpc-20.id}"
  cidr_block = "10.20.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-20.tags.Name}-sub-a"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-20-sub-b" {
  vpc_id     = "${aws_vpc.vpc-20.id}"
  cidr_block = "10.20.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-20.tags.Name}-sub-b"
    Owner = "${var.USERNAME}"
  }
}


resource "aws_subnet" "vpc-30-sub-a" {
  vpc_id     = "${aws_vpc.vpc-30.id}"
  cidr_block = "10.30.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-30.tags.Name}-sub-b"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_subnet" "vpc-30-sub-b" {
  vpc_id     = "${aws_vpc.vpc-30.id}"
  cidr_block = "10.30.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-30.tags.Name}-sub-b"
    Owner = "${var.USERNAME}"
  }
}

# Internet Gateway

resource "aws_internet_gateway" "vpc-1-igw" {
  vpc_id = "${aws_vpc.vpc-1.id}"

  tags = {
    Name = "vpc-1-igw"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

# Main Route Tables Associations
## Forcing our Route Tables to be the main ones for our VPCs,
## otherwise AWS automatically will create a main Route Table
## for each VPC, leaving our own Route Tables as secondary

resource "aws_main_route_table_association" "main-rt-vpc-1" {
  vpc_id         = "${aws_vpc.vpc-1.id}"
  route_table_id = "${aws_route_table.vpc-1-rtb.id}"
}

resource "aws_main_route_table_association" "main-rt-vpc-10" {
  vpc_id         = "${aws_vpc.vpc-10.id}"
  route_table_id = "${aws_route_table.vpc-10-rtb.id}"
}

resource "aws_main_route_table_association" "main-rt-vpc-20" {
  vpc_id         = "${aws_vpc.vpc-20.id}"
  route_table_id = "${aws_route_table.vpc-20-rtb.id}"
}

resource "aws_main_route_table_association" "main-rt-vpc-30" {
  vpc_id         = "${aws_vpc.vpc-30.id}"
  route_table_id = "${aws_route_table.vpc-30-rtb.id}"
}

# Route Tables
## Usually unecessary to explicitly create a Route Table in Terraform
## since AWS automatically creates and assigns a 'Main Route Table'
## whenever a VPC is created. However, in a Transit Gateway scenario,
## Route Tables are explicitly created so an extra route to the
## Transit Gateway could be defined

resource "aws_route_table" "vpc-1-rtb" {
  vpc_id = "${aws_vpc.vpc-1.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.vpc-1-igw.id}"
  }

  tags = {
    Name       = "vpc-1-rtb"
    env        = "shared"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_route_table" "vpc-10-rtb" {
  vpc_id = "${aws_vpc.vpc-10.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }

  tags = {
    Name       = "vpc-10-rtb"
    env        = "dev"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_route_table" "vpc-20-rtb" {
  vpc_id = "${aws_vpc.vpc-20.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }

  tags = {
    Name       = "vpc-20-rtb"
    env        = "preprod"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_route_table" "vpc-30-rtb" {
  vpc_id = "${aws_vpc.vpc-30.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  }  

  tags = {
    Name       = "vpc-30-rtb"
    env        = "prod"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}


###########################
# Transit Gateway Section #
###########################

# Transit Gateway
## Default association and propagation are disabled since our scenario involves
## a more elaborated setup where
## - Dev VPCs can reach themselves and the Shared VPC
## - the Shared VPC can reach all VPCs
## - the Prod VPC can only reach the Shared VPC
## The default setup being a full mesh scenario where all VPCs can see every other
resource "aws_ec2_transit_gateway" "sca-tgw" {
  description                     = "Transit Gateway testing scenario with 4 VPCs, 2 subnets each"
#  default_route_table_association = "disable"
#  default_route_table_propagation = "disable"
  tags                            = {
    Name                          = "${var.scenario}"
    scenario                      = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

# VPC attachment

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-1" {
  subnet_ids         = ["${aws_subnet.vpc-1-sub-priv-a.id}", "${aws_subnet.vpc-1-sub-priv-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  vpc_id             = "${aws_vpc.vpc-1.id}"
#  transit_gateway_default_route_table_association = false
#  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc1"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-10" {
  subnet_ids         = ["${aws_subnet.vpc-10-sub-a.id}", "${aws_subnet.vpc-10-sub-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  vpc_id             = "${aws_vpc.vpc-10.id}"
#  transit_gateway_default_route_table_association = false
#  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc10"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-20" {
  subnet_ids         = ["${aws_subnet.vpc-20-sub-a.id}", "${aws_subnet.vpc-20-sub-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  vpc_id             = "${aws_vpc.vpc-20.id}"
#  transit_gateway_default_route_table_association = false
#  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc20"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-30" {
  subnet_ids         = ["${aws_subnet.vpc-30-sub-a.id}", "${aws_subnet.vpc-30-sub-a.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  vpc_id             = "${aws_vpc.vpc-30.id}"
#  transit_gateway_default_route_table_association = false
#  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc30"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

# Route Tables

resource "aws_ec2_transit_gateway_route_table" "tgw-vpc-1-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  tags               = {
    Name             = "tgw-vpc-1-rt"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_route_table" "tgw-vpc-10-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  tags               = {
    Name             = "tgw-vpc-10-rt"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_route_table" "tgw-vpc-20-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  tags               = {
    Name             = "tgw-vpc-20-rt"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

resource "aws_ec2_transit_gateway_route_table" "tgw-vpc-30-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.sca-tgw.id}"
  tags               = {
    Name             = "tgw-vpc-30-rt"
    scenario         = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
  depends_on = ["aws_ec2_transit_gateway.sca-tgw"]
}

# Route Tables Associations
## This is the link between a VPC (already symbolized with its attachment to the Transit Gateway)
##  and the Route Table the VPC's packet will hit when they arrive into the Transit Gateway.
## The Route Tables Associations do not represent the actual routes the packets are routed to.
## These are defined in the Route Tables Propagations section below.

#resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-1-assoc" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-vpc-1-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-10-assoc" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-10.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-vpc-10-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-20-assoc" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-20.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-vpc-20-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-30-assoc" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-30.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-vpc-30-rt.id}"
#}

## Route Tables Propagations
### This section defines which VPCs will be routed from each Route Table created in the Transit Gateway
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-1" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-2" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-2.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
#}
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-3" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-3.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-shared-to-vpc-1" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-shared-to-vpc-2" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-2.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-shared-to-vpc-4" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-4.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
#}
#
#resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-prod-to-vpc-3" {
#  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-3.id}"
#  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-prod-rt.id}"
#}

#########################
# EC2 Instances Section #
#########################

# Key Pair

resource "aws_key_pair" "sca-tgw-keypair" {
  key_name   = "sca-tgw-keypair"
  public_key = "${var.PUBLIC_KEY}"
}

# Security Groups
## Need to create 4 of them as our Security Groups are linked to a VPC

resource "aws_security_group" "sec-group-vpc-1-ssh-icmp" {
  name        = "sec-group-vpc-1-ssh-icmp"
  description = "sca-tgw: Allow SSH and ICMP traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo Reply'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-1-ssh-icmp"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_security_group" "sec-group-vpc-10" {
  name        = "sec-group-vpc-10"
  description = "sca-tgw: Allow ALL"
  vpc_id      = "${aws_vpc.vpc-10.id}"

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-10"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_security_group" "sec-group-vpc-20" {
  name        = "sec-group-vpc-20"
  description = "sca-tgw: Allow ALL"
  vpc_id      = "${aws_vpc.vpc-20.id}"

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-20"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_security_group" "sec-group-vpc-30" {
  name        = "sec-group-vpc-30"
  description = "sca-tgw: Allow ALL"
  vpc_id      = "${aws_vpc.vpc-30.id}"

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-30"
    scenario = "${var.scenario}"
    Owner = "${var.USERNAME}"
  }
}

# VMs

## Fetching AMI info
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "sca-tgw-instance1-shared" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.vpc-1-sub-pub-a.id}"
  vpc_security_group_ids     = [ "${aws_security_group.sec-group-vpc-1-ssh-icmp.id}" ]
  key_name                    = "${aws_key_pair.sca-tgw-keypair.key_name}"
  private_ip                  = "10.1.1.10"
  associate_public_ip_address = true

  tags = {
    Name = "sca-tgw-instance1-shared"
    scenario    = "${var.scenario}"
    env         = "shared"
    az          = "${var.az1}"
    vpc         = "1"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_instance" "sca-tgw-instance2-dev" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.vpc-10-sub-a.id}"
  vpc_security_group_ids     = [ "${aws_security_group.sec-group-vpc-10.id}" ]
  key_name                    = "${aws_key_pair.sca-tgw-keypair.key_name}"
  private_ip                  = "10.10.1.10"

  tags = {
    Name = "sca-tgw-instance2-dev"
    scenario    = "${var.scenario}"
    env         = "dev"
    az          = "${var.az1}"
    vpc         = "10"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_instance" "sca-tgw-instance3-preprod" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.vpc-20-sub-a.id}"
  vpc_security_group_ids     = [ "${aws_security_group.sec-group-vpc-20.id}" ]
  key_name                    = "${aws_key_pair.sca-tgw-keypair.key_name}"
  private_ip                  = "10.20.1.10"

  tags = {
    Name = "sca-tgw-instance3-shared"
    scenario    = "${var.scenario}"
    env         = "shared"
    az          = "${var.az1}"
    vpc         = "3"
    Owner = "${var.USERNAME}"
  }
}

resource "aws_instance" "sca-tgw-instance4-prod" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.vpc-30-sub-a.id}"
  vpc_security_group_ids     = [ "${aws_security_group.sec-group-vpc-30.id}" ]
  key_name                    = "${aws_key_pair.sca-tgw-keypair.key_name}"
  private_ip                  = "10.30.1.10"

  tags = {
    Name = "sca-tgw-instance4-prod"
    scenario    = "${var.scenario}"
    env         = "prod"
    az          = "${var.az1}"
    vpc         = "30"
    Owner = "${var.USERNAME}"
  }
}

###########
# Outputs #
###########

output "PUBLIC_IP" { value = "${aws_instance.sca-tgw-instance1-shared.public_ip}" }
